from pyramid.config import Configurator
from pyramid.session import UnencryptedCookieSessionFactoryConfig

session_factory = UnencryptedCookieSessionFactoryConfig('itsaseekreet')

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.session_factory = session_factory
    
    config.add_static_view('static', 'ng_mashup:static', cache_max_age=3600)
    config.add_route('home', '/')
    config.scan()
    return config.make_wsgi_app()
