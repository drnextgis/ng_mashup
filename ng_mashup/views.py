from pyramid.view import view_config
from pyramid.response import Response

@view_config(route_name='home', renderer='main.mako')
def base_view(request):
    return dict(url=request.POST.get('url', ''),
                label=request.POST.get('label', ''),
                layer=request.POST.get('layer', ''))