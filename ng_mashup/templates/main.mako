<!DOCTYPE html>
<html>
<head>
    <title>NG Mashup</title>
    <link rel="stylesheet" href="${request.static_url('ng_mashup:static/css/map.css')}" type="text/css">
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?v=3.6&sensor=false"></script>
    <script src="http://openlayers.org/dev/OpenLayers.js"></script>
</head>

<body onload="init()">
    <div id="map"></div>
    <script>
        epsg4326 = new OpenLayers.Projection('EPSG:4326');
        epsg900913 = new OpenLayers.Projection('EPSG:900913');

        function init() {
            var extent = new OpenLayers.Bounds.fromString('102.71989,68.45120,107.4601,74.548663').transform(epsg4326, epsg900913);

            map = new OpenLayers.Map('map', {
                units: 'm',
                displayProjection: epsg4326,
                maxExtent: new OpenLayers.Bounds(-20037508.34,-20037508.34,20037508.34,20037508.34),
                maxResolution: 156543.0399,
                numZoomLevels: 19
            });

            var lay_goo = new OpenLayers.Layer.Google('Google',
                {
                    type: google.maps.MapTypeId.SATELLITE,
                    numZoomLevels: 22,
                    sphericalMercator: true,
                    visible: true, 
                    opacity: 0.5
                }
            );

            % if url:
                map.addLayer(new OpenLayers.Layer.TMS(
                        "${ label }", "${ url }",
                        {
                            type: "png",
                            layername: "${ layer }",
                            isBaseLayer: false,
                            maxExtent: new OpenLayers.Bounds(11187803.9301490411162376, 11504692.4330116957426071, 12080221.0529731623828411, 12166950.5943169482052326),
                            units: 'm',
                            serverResolutions: [156543.03390625, 78271.516953125, 39135.7584765625, 19567.87923828125, 9783.939619140625, 4891.9698095703125, 2445.9849047851562, 1222.9924523925781, 611.4962261962891, 305.74811309814453, 152.87405654907226, 76.43702827453613, 38.218514137268066, 19.109257068634033, 9.554628534317017],
                            resolutions: [4891.9698095703125, 2445.9849047851562, 1222.9924523925781, 611.4962261962891, 305.74811309814453, 152.87405654907226, 76.43702827453613, 38.218514137268066, 19.109257068634033, 9.554628534317017],
                            visibility: true
                        }
                    ));                
            % endif

            map.addControl(new OpenLayers.Control.LayerSwitcher());
            map.addLayer(lay_goo);
            map.zoomToExtent(extent);
        }
    </script>
    <form method='post'>
        <label>URL</label><input name='url' value = "${url}"><br>
        <label>Layer</label><input name='layer' value = "${layer}"><br>
        <label>Label</label><input name='label'value = "${label}"><br>
        <input type='submit'>
    </form>
</body>
</html>